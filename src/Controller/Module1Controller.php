<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class Module1Controller extends AbstractController
{
    /**
     * @Route("/cpm3000", name="cpm_index")
     */
    public function index(): Response
    {
        return new Response('Bienvenue Module CPM 3000');
    }
}